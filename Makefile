
all: sni

sni: main.cpp
	g++ -std=c++17 main.cpp -o sniffer -lpcap -g0 -O3 -Wall -Wextra -pedantic

run:
	./sniffer

