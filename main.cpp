
#include <iostream>
#include <cstring>
#include <cassert>
#include <sstream>
#include <memory>
#include <vector>
#include <deque>
#include <map>
#include <pcap.h>
#include "logger.h"



////////////////////////////////////////////////////////////////////////////////

std::string hex(const void* ptr, size_t len) {
  std::stringstream sstream;
  sstream << "{ " << std::hex;
  for (size_t i{}; i < len; ++i) {
    sstream << "0x" << (int) ((const uint8_t*) ptr)[i] << " ";
  }
  sstream << "} " << std::dec << len << "s";
  return sstream.str();
}

void ntoh_base(uint8_t* value, size_t len)
{
  char tmp;
  for (size_t i{}; i < len / 2; ++i) {
    tmp = value[i];
    value[i] = value[len - 1 - i];
    value[len - 1 - i] = tmp;
  }
}

template <typename T>
void ntoh(T& value) {
  ntoh_base((uint8_t*) &value, sizeof(value));
}

////////////////////////////////////////////////////////////////////////////////

struct decoder_t;

struct stream_t { // old
  virtual ~stream_t() { }
  virtual size_t size() const = 0;
  virtual size_t pos() const = 0;
  virtual void set_limit(size_t len) = 0;
  virtual void skip(size_t len) = 0;
  virtual void read_data(void* buff, size_t len) = 0;
  virtual void add_data(const void* buff, size_t len) = 0;
  virtual void move_data(stream_t& stream) = 0;
};

struct bstream_t : stream_t {
  std::vector<uint8_t> _buff;
  size_t _pos;
  size_t _limit;

  bstream_t() : _pos(0), _limit(0) { }
  size_t size() const override;
  size_t pos() const override;
  void set_limit(size_t len) override;
  void skip(size_t len) override;
  void read_data(void* buff, size_t len) override;
  void add_data(const void* buff, size_t len) override;
  void move_data(stream_t& stream) override;
  void clear_maybe();
};

size_t bstream_t::size() const {
  return _limit - _pos;
}

size_t bstream_t::pos() const {
  return _pos;
}

void bstream_t::set_limit(size_t limit) {
  LOGGER_FLOW;
  if (_limit >= limit) {
    LOG_FLOW("_limit: %zd", _limit);
    LOG_FLOW("limit:  %zd", limit);
    _limit = limit;
  } else {
    throw std::runtime_error("empty flow");
  }
}

void bstream_t::skip(size_t len) {
  LOGGER_FLOW;
  LOG_FLOW("size() : %zu", size());
  LOG_FLOW("len    : %zu", len);
  if (size() >= len) {
    LOG_FLOW("skip: %s", hex(_buff.data() + _pos, len).c_str());
    _pos += len;
  } else {
    throw std::runtime_error("empty flow");
  }
}

void bstream_t::read_data(void* buff, size_t len) {
  LOGGER_FLOW;
  LOG_FLOW("size(): %zd", size());
  LOG_FLOW("len:    %zd", len);
  if (size() >= len) {
    memcpy(buff, _buff.data() + _pos, len);
    _pos += len;
    LOG_FLOW("read: %s", hex(buff, len).c_str());
  } else {
    throw std::runtime_error("empty flow");
  }
}

void bstream_t::add_data(const void* buff, size_t len) {
  LOGGER_FLOW;
  assert(_pos <= _buff.size());
  assert(_limit <= _buff.size());
  clear_maybe();
  _buff.insert(_buff.begin() + _pos, (uint8_t*) buff, len + (uint8_t*) buff);
  _limit += len;
}

void bstream_t::move_data(stream_t& stream) {
  LOGGER_FLOW;
  size_t size = stream.size();
  std::vector<uint8_t> buff(size);
  stream.read_data(buff.data(), size);
  add_data(buff.data(), size);
}

void bstream_t::clear_maybe() {
  LOGGER_FLOW;
  if (_pos > 1000) {
    LOG_FLOW("_pos: %zd", _pos);
    _buff.erase(_buff.begin(), _buff.begin() + _pos);
    _limit -= _pos;
    _pos -= _pos;
  }
}

////////////////////////////////////////////////////////////////////////////////

struct flow_key_t {
  uint32_t _src_ip;
  uint16_t _src_port;
  uint32_t _dst_ip;
  uint16_t _dst_port;

  flow_key_t reverse();
};

flow_key_t flow_key_t::reverse() {
  return flow_key_t {_dst_ip, _dst_port, _src_ip, _src_port};
}

bool operator==(const flow_key_t& l, const flow_key_t& r) {
  return std::tie(l._src_ip, l._src_port, l._dst_ip, l._dst_port) ==
    std::tie(r._src_ip, r._src_port, r._dst_ip, r._dst_port);
}

bool operator <(const flow_key_t& l, const flow_key_t& r) {
  return std::tie(l._src_ip, l._src_port, l._dst_ip, l._dst_port) <
    std::tie(r._src_ip, r._src_port, r._dst_ip, r._dst_port);
}

////////////////////////////////////////////////////////////////////////////////

struct flow_info_t {
  int _level_next;
  int _type_next;
  uint32_t _ip[2];
  uint16_t _port[2];
  uint32_t _seq;
  uint32_t _ack;

  flow_info_t() : _level_next(2), _type_next(0) { }
  flow_key_t key();
};

flow_key_t flow_info_t::key() {
  return flow_key_t{_ip[0], _port[0], _ip[1], _port[1]};
}

////////////////////////////////////////////////////////////////////////////////

struct flow_t { // old
  virtual ~flow_t() { }
  virtual size_t size() const = 0;
  virtual size_t pos() const = 0;
  virtual void set_limit(size_t len) = 0;
  virtual void skip(size_t len) = 0;
  virtual void read_data(void* buff, size_t len) = 0;
  virtual void add_data(const void* buff, size_t len) = 0;
  virtual flow_info_t& get_info() = 0;
};

struct flow_l7_t : flow_t {
  bstream_t _stream[2];
  flow_info_t _info;
  int _side;
  // std::shared_ptr<decoder_t> _decoder; TODO

  flow_l7_t() : _side(0) { }
  void add_flow(flow_l7_t& flow);
  bool validate_flow(flow_l7_t& flow);
  void switch_side() { _side = (_side + 1) % 2; }
  void set_side(int side) { assert(side == 0 || side == 1); _side = side; }

  size_t size() const override                          { return _stream[_side].size(); }
  size_t pos() const override                           { return _stream[_side].pos(); }
  void set_limit(size_t len) override                   { _stream[_side].set_limit(len); }
  void skip(size_t len) override                        { _stream[_side].skip(len); }
  void read_data(void* buff, size_t len) override       { _stream[_side].read_data(buff, len); }
  void add_data(const void* buff, size_t len) override  { _stream[_side].add_data(buff, len); }
  flow_info_t& get_info() override                      { return _info; }
};

bool flow_l7_t::validate_flow(flow_l7_t& flow) {
  LOGGER_FLOW;

  bool s = get_info().key() == flow.get_info().key();
  uint32_t& seq = s ? get_info()._seq : get_info()._ack;
  uint32_t& ack = s ? get_info()._ack : get_info()._seq;
  uint32_t& fseq = flow.get_info()._seq;
  uint32_t& fack = flow.get_info()._ack;

  if (!seq || !ack) {
    LOG_FLOW("set seq ack");
    seq = fseq + flow.size();
    ack = fack;
    return true;
  }

  LOG_FLOW("port: %u %u", get_info()._port[0], flow.get_info()._port[0]);
  LOG_FLOW("seq:  %u  %u", seq, fseq);
  LOG_FLOW("ack:  %u  %u", ack, fack);
  LOG_FLOW("size: %u  %u  %u", _stream[0].size(), _stream[1].size(), flow.size());

  if (seq > fseq) {
    LOG_FLOW("WARN: seq > flow._seq: %u %u", seq, fseq);
    return false;
  } else if (seq + 1 == fseq) {
    LOG_FLOW("WARN: seq + 1 == flow._seq: %u %u", seq, fseq); // XXX TODO Не учли маленький пакет.
    seq = fseq;
    ack = fack;
  } else if (seq != fseq) {
    LOG_FLOW("ERROR: seq != flow._seq: %u %u", seq, fseq);
    seq = fseq + flow.size();
    ack = fack;
    return false;
  }
  seq += flow.size();
  return true;
}

void flow_l7_t::add_flow(flow_l7_t& flow) {
  LOGGER_FLOW;
  assert(!flow._stream[1].size());
  if (get_info().key() == flow.get_info().key()) {
    LOGGER_FLOW;
    if (validate_flow(flow)) {
      _stream[0].move_data(flow._stream[0]);
    }
  } else if (get_info().key() == flow.get_info().key().reverse()) {
    LOGGER_FLOW;
    if (validate_flow(flow)) {
      _stream[1].move_data(flow._stream[0]);
    }
  } else {
    LOGGER_FLOW;
    get_info() = flow.get_info();
    if (validate_flow(flow)) {
      _stream[0].move_data(flow._stream[0]);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

struct ctx_empty_t { };

template <typename tctx_t, typename tgctx_t>
struct field_t {
  using ctx_t = tctx_t;
  field_t() {
    LOGGER_FIELD;
  }
  virtual ~field_t() {
    LOGGER_FIELD;
  }
  virtual bool taste(flow_l7_t& /*flow*/, tctx_t& /*ctx*/, tgctx_t& /*gctx*/) const {
    LOGGER_FIELD;
    return true;
  }
  virtual void read(flow_l7_t& /*flow*/, tctx_t& /*ctx*/, tgctx_t& /*gctx*/) const {
    LOGGER_FIELD;
  }
  virtual void process(flow_l7_t& /*flow*/, tctx_t& /*ctx*/) const {
    LOGGER_FIELD;
  }
  virtual bool decode(flow_l7_t& flow, tctx_t& ctx, tgctx_t& gctx) const {
    LOGGER_FIELD;
    if (taste(flow, ctx, gctx)) {
      read(flow, ctx, gctx);
      process(flow, ctx);
      return true;
    } else {
      return false;
    }
  }
};

template <typename tctx_t, typename tgctx_t>
using field_single_t = field_t<tctx_t, tgctx_t>; // old

template <typename tctx_t, typename tgctx_t, typename f_single_t, auto value>
struct field_single_read_t : field_single_t<tctx_t, tgctx_t> {
  void read(flow_l7_t& flow, tctx_t& ctx, tgctx_t& gctx) const override {
    LOGGER_FIELD;
    static const f_single_t field;
    field.read(flow, ctx.*value, gctx);
  }
};

template <typename tctx_t, typename tgctx_t>
struct field_sequence_t : field_t<tctx_t, tgctx_t> {
  std::vector<std::shared_ptr<field_t<tctx_t, tgctx_t>>> fields;

  virtual bool decode(flow_l7_t& flow, tctx_t& /*ctx*/, tgctx_t& gctx) const override {
    LOGGER_FIELD;
    // state_field_t<context_t> state;
    tctx_t ctx = *std::make_shared<tctx_t>(); // XXX to state
    if (this->taste(flow, ctx, gctx)) {
      this->read(flow, ctx, gctx);
      for (auto& field : fields) {
        if (!field->decode(flow, ctx, gctx)) {
          return false;
        }
      }
      this->process(flow, ctx);
    }
    return true;
  }
};

template <typename tctx_t, typename tgctx_t>
struct field_any_t : field_t<tctx_t, tgctx_t> {
  std::vector<std::shared_ptr<field_t<tctx_t, tgctx_t>>> fields;

  virtual bool decode(flow_l7_t& flow, tctx_t& ctx, tgctx_t& gctx) const override {
    LOGGER_FIELD;
    // state_field_t<context_t> state;
    bool ret = false;
    if (this->taste(flow, ctx, gctx)) {
      this->read(flow, ctx, gctx);
      // XXX while
      for (auto& field : fields) {
        if (field->decode(flow, ctx, gctx)) {
          ret = true;
        }
      }
      this->process(flow, ctx);
    }
    return ret;
  }
};

template <typename field_t, typename tctx_t, typename tgctx_t>
struct field_as_single_t : field_single_t<ctx_empty_t, tgctx_t> {
  bool decode(flow_l7_t& flow, ctx_empty_t& /*ctx*/, tgctx_t& gctx) const override {
    LOGGER_FIELD;
    static tctx_t ctx; // XXX not use. save prototype.
    static const field_t field;
    return field.decode(flow, ctx, gctx);
  }
};

////////////////////////////////////////////////////////////////////////////////

template <typename tctx_t, typename tgctx_t, size_t len>
struct field_array_skip_t : field_single_t<tctx_t, tgctx_t> {
  void read(flow_l7_t& flow, tctx_t& /*ctx*/, tgctx_t& /*gctx*/) const override {
    LOGGER_FIELD;
    flow.skip(len);
  }
};

template <typename tctx_t, typename tgctx_t>
using field_uint8_skip_t = field_array_skip_t<tctx_t, tgctx_t, sizeof(uint8_t)>;

template <typename tctx_t, typename tgctx_t>
using field_uint16_skip_t = field_array_skip_t<tctx_t, tgctx_t, sizeof(uint16_t)>;

template <typename tctx_t, typename tgctx_t>
using field_uint32_skip_t = field_array_skip_t<tctx_t, tgctx_t, sizeof(uint32_t)>;



template <typename tgctx_t>
struct field_uint8_t : field_single_t<uint8_t, tgctx_t> {
  using ctx_t = typename field_single_t<uint8_t, tgctx_t>::ctx_t;
  void read(flow_l7_t& flow, ctx_t& value, tgctx_t& /*gctx*/) const override {
    LOGGER_FIELD;
    flow.read_data(&value, sizeof(value));
  }
};

template <typename tgctx_t>
struct field_uint16_hbo_t : field_single_t<uint16_t, tgctx_t> {
  using ctx_t = typename field_single_t<uint16_t, tgctx_t>::ctx_t;
  void read(flow_l7_t& flow, ctx_t& value, tgctx_t& /*gctx*/) const override {
    LOGGER_FIELD;
    flow.read_data(&value, sizeof(value));
    ntoh(value);
  }
};

template <typename tgctx_t>
struct field_uint32_hbo_t : field_single_t<uint32_t, tgctx_t> {
  using ctx_t = typename field_single_t<uint32_t, tgctx_t>::ctx_t;
  void read(flow_l7_t& flow, ctx_t& value, tgctx_t& /*gctx*/) const override {
    LOGGER_FIELD;
    flow.read_data(&value, sizeof(value));
    ntoh(value);
  }
};

////////////////////////////////////////////////////////////////////////////////

struct context_eth_t {
  // uint8_t mac_dst[6];
  // uint8_t mac_src[6];
  uint16_t ether_type;
};

template <typename tgctx_t>
using field_eth_mac_skip_t = field_array_skip_t<context_eth_t, tgctx_t, 6 * sizeof(uint8_t)>;

template <typename tgctx_t>
using field_eth_type = field_single_read_t<context_eth_t, tgctx_t,
    field_uint16_hbo_t<tgctx_t>, &context_eth_t::ether_type>;

template <typename tgctx_t>
struct field_eth_t : field_sequence_t<context_eth_t, tgctx_t> {
  using ctx_t = typename field_single_t<context_eth_t, tgctx_t>::ctx_t;
  field_eth_t() {
    LOGGER_FIELD;
    this->fields.push_back(std::make_shared<field_eth_mac_skip_t<tgctx_t>>()); // mac_dst
    this->fields.push_back(std::make_shared<field_eth_mac_skip_t<tgctx_t>>()); // mac_src
    this->fields.push_back(std::make_shared<field_eth_type<tgctx_t>>());       // ether_type
  }
  bool taste(flow_l7_t& flow, ctx_t& /*ctx*/, tgctx_t& /*gctx*/) const override {
    LOGGER_FIELD;
    return flow.get_info()._level_next == 2 && flow.get_info()._type_next == 0;
  }
  void process(flow_l7_t& flow, ctx_t& ctx) const override {
    LOGGER_FIELD;
    flow.get_info()._type_next = ctx.ether_type;
    flow.get_info()._level_next++;
  }
};

////////////////////////////////////////////////////////////////////////////////

struct context_ipv4_t {
  uint8_t version;
  // uint8_t type_service;
  uint16_t total_length;
  // uint16_t identification;
  // uint16_t flags;
  // uint8_t ttl;
  uint8_t protocol;
  // uint16_t checksum;
  uint32_t src_ip;
  uint32_t dst_ip;
  // options;

  uint8_t get_version() { return (version & 0xF0) >> 4; }
  uint8_t get_hlen() { return version & 0x0F; }
};

template <typename tgctx_t>
using field_ipv4_version_t  = field_single_read_t<context_ipv4_t, tgctx_t,
    field_uint8_t<tgctx_t>, &context_ipv4_t::version>;

template <typename tgctx_t>
using field_ipv4_service_t  = field_uint8_skip_t<context_ipv4_t, tgctx_t>;

template <typename tgctx_t>
using field_ipv4_tlen_t     = field_single_read_t<context_ipv4_t, tgctx_t,
    field_uint16_hbo_t<tgctx_t>, &context_ipv4_t::total_length>;

template <typename tgctx_t>
using field_ipv4_ident_t    = field_uint16_skip_t<context_ipv4_t, tgctx_t>;

template <typename tgctx_t>
using field_ipv4_flags_t    = field_uint16_skip_t<context_ipv4_t, tgctx_t>;

template <typename tgctx_t>
using field_ipv4_ttl_t      = field_uint8_skip_t<context_ipv4_t, tgctx_t>;

template <typename tgctx_t>
using field_ipv4_protocol_t = field_single_read_t<context_ipv4_t, tgctx_t,
    field_uint8_t<tgctx_t>, &context_ipv4_t::protocol>;

template <typename tgctx_t>
using field_ipv4_checksum_t = field_uint16_skip_t<context_ipv4_t, tgctx_t>;

template <typename tgctx_t>
using field_ipv4_src_ip_t   = field_single_read_t<context_ipv4_t, tgctx_t,
    field_uint32_hbo_t<tgctx_t>, &context_ipv4_t::src_ip>;

template <typename tgctx_t>
using field_ipv4_dst_ip_t   = field_single_read_t<context_ipv4_t, tgctx_t,
    field_uint32_hbo_t<tgctx_t>, &context_ipv4_t::dst_ip>;

template <typename tgctx_t>
struct field_ipv4_options_t : field_single_t<context_ipv4_t, tgctx_t> {
  using ctx_t = typename field_single_t<context_ipv4_t, tgctx_t>::ctx_t;
  void read(flow_l7_t& flow, ctx_t& ctx, tgctx_t& /*gctx*/) const override {
    LOGGER_FIELD;
    flow.skip((ctx.get_hlen() - 5) * sizeof(uint32_t));
  }
};

template <typename tgctx_t>
struct field_ipv4_t : field_sequence_t<context_ipv4_t, tgctx_t> {
  using ctx_t = typename field_sequence_t<context_ipv4_t, tgctx_t>::ctx_t;
  field_ipv4_t() {
    LOGGER_FIELD;
    this->fields.push_back(std::make_shared<field_ipv4_version_t<tgctx_t>>());  // version, hlen
    this->fields.push_back(std::make_shared<field_ipv4_service_t<tgctx_t>>());  // type_service
    this->fields.push_back(std::make_shared<field_ipv4_tlen_t<tgctx_t>>());     // total_length
    this->fields.push_back(std::make_shared<field_ipv4_ident_t<tgctx_t>>());    // identification
    this->fields.push_back(std::make_shared<field_ipv4_flags_t<tgctx_t>>());    // flags
    this->fields.push_back(std::make_shared<field_ipv4_ttl_t<tgctx_t>>());      // ttl
    this->fields.push_back(std::make_shared<field_ipv4_protocol_t<tgctx_t>>()); // protocol
    this->fields.push_back(std::make_shared<field_ipv4_checksum_t<tgctx_t>>()); // checksum
    this->fields.push_back(std::make_shared<field_ipv4_src_ip_t<tgctx_t>>());   // src_ip
    this->fields.push_back(std::make_shared<field_ipv4_dst_ip_t<tgctx_t>>());   // dst_ip
    this->fields.push_back(std::make_shared<field_ipv4_options_t<tgctx_t>>());  // options
  }

  bool taste(flow_l7_t& flow, ctx_t& /*ctx*/, tgctx_t& /*gctx*/) const override {
    LOGGER_FIELD;
    return flow.get_info()._level_next == 3 && flow.get_info()._type_next == 0x0800;
  }

  void process(flow_l7_t& flow, ctx_t& ctx) const override {
    LOGGER_FIELD;
    flow.get_info()._type_next = ctx.protocol;
    flow.get_info()._ip[0] = ctx.src_ip;
    flow.get_info()._ip[1] = ctx.dst_ip;
    flow.set_limit(flow.pos() - 20 * sizeof(uint8_t)
        - (ctx.get_hlen() - 5) * sizeof(uint32_t) + ctx.total_length); // XXX preprocessing
    flow.get_info()._level_next++;
  }
};

////////////////////////////////////////////////////////////////////////////////

struct context_tcp_t {
  uint16_t src_port;
  uint16_t dst_port;
  uint32_t seq;
  uint32_t ack;
  uint16_t flags;
  // uint16_t window_size;
  // uint16_t checksum;
  // uint16_t urgent_pointer;
  // options;

  uint8_t get_data_offset() { return (flags >> 12) & 0xFFFF; }
};

template <typename tgctx_t>
using field_tcp_src_port_t = field_single_read_t<context_tcp_t, tgctx_t,
    field_uint16_hbo_t<tgctx_t>, &context_tcp_t::src_port>;

template <typename tgctx_t>
using field_tcp_dst_port_t = field_single_read_t<context_tcp_t, tgctx_t,
    field_uint16_hbo_t<tgctx_t>, &context_tcp_t::dst_port>;

template <typename tgctx_t>
using field_tcp_seq_t = field_single_read_t<context_tcp_t, tgctx_t,
    field_uint32_hbo_t<tgctx_t>, &context_tcp_t::seq>;

template <typename tgctx_t>
using field_tcp_ack_t = field_single_read_t<context_tcp_t, tgctx_t,
    field_uint32_hbo_t<tgctx_t>, &context_tcp_t::ack>;

template <typename tgctx_t>
using field_tcp_flags_t = field_single_read_t<context_tcp_t, tgctx_t,
    field_uint16_hbo_t<tgctx_t>, &context_tcp_t::flags>;

template <typename tgctx_t>
using field_tcp_win_size_t = field_uint16_skip_t<context_tcp_t, tgctx_t>;

template <typename tgctx_t>
using field_tcp_checksum_t = field_uint16_skip_t<context_tcp_t, tgctx_t>;

template <typename tgctx_t>
using field_tcp_urgent_ptr_t = field_uint16_skip_t<context_tcp_t, tgctx_t>;

template <typename tgctx_t>
struct field_tcp_options_t : field_single_t<context_tcp_t, tgctx_t> {
  using ctx_t = typename field_single_t<context_tcp_t, tgctx_t>::ctx_t;
  void read(flow_l7_t& flow, ctx_t& ctx, tgctx_t& /*gctx*/) const override {
    LOGGER_FIELD;
    flow.skip((ctx.get_data_offset() - 5) * sizeof(uint32_t));
  }
};

template <typename tgctx_t>
struct field_tcp_t : field_sequence_t<context_tcp_t, tgctx_t> {
  using ctx_t = typename field_sequence_t<context_tcp_t, tgctx_t>::ctx_t;
  field_tcp_t() {
    LOGGER_FIELD;
    this->fields.push_back(std::make_shared<field_tcp_src_port_t<tgctx_t>>());   // src_port
    this->fields.push_back(std::make_shared<field_tcp_dst_port_t<tgctx_t>>());   // dst_port
    this->fields.push_back(std::make_shared<field_tcp_seq_t<tgctx_t>>());        // seq
    this->fields.push_back(std::make_shared<field_tcp_ack_t<tgctx_t>>());        // ack
    this->fields.push_back(std::make_shared<field_tcp_flags_t<tgctx_t>>());      // flags
    this->fields.push_back(std::make_shared<field_tcp_win_size_t<tgctx_t>>());   // window_size
    this->fields.push_back(std::make_shared<field_tcp_checksum_t<tgctx_t>>());   // checksum
    this->fields.push_back(std::make_shared<field_tcp_urgent_ptr_t<tgctx_t>>()); // urgent_pointer
    this->fields.push_back(std::make_shared<field_tcp_options_t<tgctx_t>>());    // options
  }
  bool taste(flow_l7_t& flow, ctx_t& /*ctx*/, tgctx_t& /*gctx*/) const override {
    LOGGER_FIELD;
    return flow.get_info()._level_next == 4 && flow.get_info()._type_next == 6;
  }
  void process(flow_l7_t& flow, ctx_t& ctx) const override {
    LOGGER_FIELD;
    flow.get_info()._port[0] = ctx.src_port;
    flow.get_info()._port[1] = ctx.dst_port;
    flow.get_info()._seq = ctx.seq;
    flow.get_info()._ack = ctx.ack;
    flow.get_info()._type_next = 0;
    flow.get_info()._level_next = 7;
  }
};


template <typename tgctx_t>
struct field_low_level_t : field_any_t<ctx_empty_t, tgctx_t> {
  using ctx_t = typename field_single_t<ctx_empty_t, tgctx_t>::ctx_t;
  field_low_level_t() {
    LOGGER_FIELD;
    this->fields.push_back(std::make_shared<field_as_single_t<field_eth_t<tgctx_t>, context_eth_t, tgctx_t>>());
    this->fields.push_back(std::make_shared<field_as_single_t<field_ipv4_t<tgctx_t>, context_ipv4_t, tgctx_t>>());
    this->fields.push_back(std::make_shared<field_as_single_t<field_tcp_t<tgctx_t>, context_tcp_t, tgctx_t>>());
  }
};

////////////////////////////////////////////////////////////////////////////////

struct device_t {
  virtual ~device_t() { }
  virtual void open() = 0;
  virtual void close() = 0;
  virtual void read(flow_l7_t& flow) = 0;
};

////////////////////////////////////////////////////////////////////////////////

struct device_pcap_t : device_t {
  pcap_t* _handle;
  const std::string _file_name;

  device_pcap_t(const std::string& file_name);
  void open() override;
  void close() override;
  void read(flow_l7_t& flow) override;
};

device_pcap_t::device_pcap_t(const std::string& file_name) : _file_name(file_name) { }

void device_pcap_t::open() {
  LOGGER_PCAP;
  char error_buffer[PCAP_ERRBUF_SIZE];
  _handle = pcap_open_offline(_file_name.c_str(), error_buffer);

  if (_handle == NULL) {
    std::string err = pcap_geterr(_handle);
    throw std::runtime_error(err);
  }
}

void device_pcap_t::close() {
  LOGGER_PCAP;
  pcap_close(_handle);
}

void device_pcap_t::read(flow_l7_t& flow) {
  LOGGER_PCAP;

  struct pcap_pkthdr* pkt_header = NULL;
  const u_char* pkt_data = NULL;

  int res = pcap_next_ex(_handle, &pkt_header, &pkt_data);
  LOG_PCAP("res: %d", res);
  if (res == 1) {
    flow.add_data(pkt_data, pkt_header->len);
    LOG_PCAP("read: %s ", hex(pkt_data, pkt_header->len).c_str());
  } else if (res == -2) {
    throw std::runtime_error("EOF");
  }
}

////////////////////////////////////////////////////////////////////////////////

struct state_t {
  int _state;

  state_t() : _state(0) {
    LOGGER_STATE;
  }
  virtual ~state_t() {
    LOGGER_STATE;
  }
};

struct stack_states_t {
  std::deque<std::shared_ptr<state_t>> _states;
  size_t _deep;

  stack_states_t() : _deep(0) { }
  void init();
  void reset();
  void end();

  template <typename state_sptr_t>
  void begin(state_sptr_t& state);
};

void stack_states_t::init() {
  LOGGER_STATE;
  _deep = 0;
}

void stack_states_t::reset() {
  LOGGER_STATE;
  _states.clear();
  _deep = 0;
}

void stack_states_t::end() {
  LOGGER_STATE;
  LOG_STATE("_deep:  %zd", _deep);
  LOG_STATE("size(): %zd", _states.size());
  if (_states.size() < _deep) {
    throw std::runtime_error("invalid size of states");
  } else if (_states.size() == _deep) {
    _states.pop_back();
    _deep--;
  } else {
    _states.resize(_deep);
  }
}

template <typename state_sptr_t>
void stack_states_t::begin(state_sptr_t& state) {
  LOGGER_STATE;
  LOG_STATE("_deep:  %zd", _deep);
  LOG_STATE("size(): %zd", _states.size());
  if (_states.size() < _deep) {
    throw std::runtime_error("invalid size of states");
  } else if (_states.size() == _deep) {
    state = std::make_shared<typename state_sptr_t::element_type>();
    _states.push_back(state);
    _deep++;
  } else {
    state = std::dynamic_pointer_cast<typename state_sptr_t::element_type>(_states[_deep]);
    assert(state);
    _deep++;
  }
}

////////////////////////////////////////////////////////////////////////////////

struct decoder_raw_t {
  virtual ~decoder_raw_t() { }
  virtual bool decode(flow_l7_t& flow) const = 0;
};

struct decoder_low_level_t : decoder_raw_t {
  bool decode(flow_l7_t& flow) const override;
};

bool decoder_low_level_t::decode(flow_l7_t& flow) const {
  ctx_empty_t ctx;
  ctx_empty_t gctx;
  static const field_low_level_t<decltype(gctx)> field;
  return field.decode(flow, ctx, gctx);
}

////////////////////////////////////////////////////////////////////////////////

struct decoder_l7_t {
  virtual ~decoder_l7_t() { }
  virtual bool taste(flow_l7_t& flow_cli, flow_l7_t& flow_srv/*, context*/) = 0;
  virtual void process(flow_l7_t& flow_cli, flow_l7_t& flow_srv/*, context*/) = 0;
};

////////////////////////////////////////////////////////////////////////////////

struct decoder_l7_teradata_t : decoder_l7_t {
  bool taste(flow_l7_t& flow_cli, flow_l7_t& flow_srv/*, context*/) override;
  void process(flow_l7_t& flow_cli, flow_l7_t& flow_srv/*, context*/) override;
};

bool decoder_l7_teradata_t::taste(flow_l7_t& /*flow_cli*/, flow_l7_t& /*flow_srv*//*, context*/) {
  return true;
}

void decoder_l7_teradata_t::process(flow_l7_t& /*flow_cli*/, flow_l7_t& /*flow_srv*//*, context*/) {
  ;
}

////////////////////////////////////////////////////////////////////////////////

struct sniffer_t {
  std::vector<std::shared_ptr<device_t>> devices;
  std::vector<std::shared_ptr<decoder_raw_t>> decoders;
  std::multimap<size_t/*level*/, std::shared_ptr<decoder_l7_t>> decoders_l7;
  std::map<flow_key_t, std::shared_ptr<flow_l7_t>> flows;

  void init();
  void process();
};

void sniffer_t::init() {
  LOGGER_SNI;
  devices.push_back(std::make_shared<device_pcap_t>("td_blob_bind_s0.pcap"));
  for (auto& device : devices) {
    device->open();
  }
  decoders.push_back(std::make_shared<decoder_low_level_t>());
  decoders_l7.insert({7, std::make_shared<decoder_l7_teradata_t>()});
}

void sniffer_t::process() {
  LOGGER_SNI;
  for (auto& device : devices) {
    std::shared_ptr<flow_l7_t> flow_raw = std::make_shared<flow_l7_t>();
    device->read(*flow_raw);

    for (auto decoder : decoders) {
      decoder->decode(*flow_raw);
    }

    if (!flow_raw->size()) {
      continue;
    }

    // l7_processing

    if (flow_raw->get_info()._level_next != 7) {
      throw std::runtime_error("expected l7 flow");
    }

    // LOG_SNI("flow_raw->_stream: %s", hex(flow_raw->_stream._buff.data()
    //     + flow_raw->_stream._pos, flow_raw->_stream.size()).c_str());

    auto p = flows.insert({flow_raw->get_info().key(), std::make_shared<flow_l7_t>()});
    if (p.second) { // key was inserted.
      flows.insert({flow_raw->get_info().key().reverse(), p.first->second});
    }

    p.first->second->add_flow(*flow_raw);
    LOG_SNI("flow_raw->size: %zd", flow_raw->size());

    std::shared_ptr<flow_l7_t> flow_l7 = p.first->second;

    // XXX test
    LOG_SNI("flow_l7->side: %d", flow_l7->_side);
    flow_l7->skip(flow_l7->size());
    flow_l7->switch_side();

    LOG_SNI("flow_l7->side: %d", flow_l7->_side);
    flow_l7->skip(flow_l7->size());
    flow_l7->switch_side();
  }
}

////////////////////////////////////////////////////////////////////////////////

int main() {
  std::cout << "start ... " << std::endl;

  try {
    sniffer_t sniffer;
    sniffer.init();
    while (true)
      sniffer.process();
  } catch (std::exception& ex) {
    std::cout << "  ERROR: " << ex.what() << std::endl;
  }

  std::cout << "end ... " << std::endl;
  return 0;
}

